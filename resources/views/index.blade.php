 @extends('app')
 @section('content')
<div class="container">
  <div class="jumbotron">
    <h3>About Us</h3>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
<form class="navbar-form navbar-left" role="search" action="/s" method="GET">
  <div class="form-group">
      <input type="text" class="form-control" name="search" placeholder="Search">
      <select class="form-control" name="category">
        <option value="">Categories</option>
        @foreach(Config::get('constants.categories') as $key => $category)
        <option value="{{$key}}">{{$category}}</option>
        @endforeach
        </select>
      <select class="form-control" name="location">
        <option value="">Locations</option>
        @foreach(Config::get('constants.locations') as $key => $location)
        <option value="{{$key}}">{{$location}}</option>
        @endforeach
      </select>
  <button type="submit" class="btn btn-default">Search</button>
</form>
  </div>
</div>
</div>
</div>
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
  <a href="#"><h2>Programming</a>
  <small></small></h2>
  <table class="table table-striped table-hover ">
  <thead>
  <tbody>
  @foreach($programmingJobs as $job)
<div class="row">
<tr>
     <div class="col-md-2 col-md-offset-1"><td>{{$job->organization->name }}</td></div>
     <div class="col-md-4"><td><a href="/show/{{$job->id}}">{{$job->title}}</a></td></div>
     <div class="col-md-5"><td>{{$job->activated_at}}</td></div>
   </tr>
      </div>
      @endforeach
    </tbody>
      </thead>
      </table>
      <a href="/all/programming" class="view-all">View all programmingjobs →</a>
  </div>
 </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
<a href="#"><h2>Design Jobs</a>
<small></small></h2>
<table class="table table-striped table-hover ">
<thead>
<tbody>
  @foreach($designJobs as $job)
  <div class="row">
<tr>
  <div class="col-md-2 col-md-offset-1"><td>{{$job->organization->name }}</td></div>
  <div class="col-md-4"><td><a href="/show/{{$job->id}}">{{ $job->title}}</a></td></div>
  <div class="col-md-5"><td>{{$job->activated_at}}</td></div>
</tr>
  </div>
  @endforeach
</tbody>
</thead>
</table>
<a href="/all/design" class="view-all">View all designjobs →</a>

</div>
</div>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
<a href="#"><h2>Marketing Jobs</a>
<small></small></h2>
<table class="table table-striped table-hover ">
<thead>
<tbody>
  @foreach($marketingJobs as $job)
  <div class="row">
<tr>
  <div class="col-md-2 col-md-offset-1"><td>{{$job->organization->name}}</td></div>
<div class="col-md-4"><td><a href="/show/{{$job->id}}">{{$job->title}}</a></td></div>
  <div class="col-md-5"><td>{{$job->activated_at}}</td></div>
</tr>
  </div>
  @endforeach


</tbody>
</thead>
</table>
<a href="/all/marketing" class="view-all">View all marketingjobs →</a>
</div>
</div>
</div>
@endsection
