@extends('app')
@section('content')
    <div class="container">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
    <h2>{{$categoryVal}}</h2>
    <small>Latest Post About 10 Hours Ago</small></h2>
    <table class="table table-striped table-hover ">
    <thead>
    <tbody>
    @foreach($jobs as $job )
    <div class="row">
    <tr>
    <div class="col-md-2 col-md-offset-1"><td>{{$job->organization->name}}</td></div>
    <div class="col-md-4"><td><a href="/show/{{$job->id}}">{{ $job->title}}</a></td></div>
    <div class="col-md-5"><td>{{$job->activated_at}}</td></div>
      </tr>
        </div>
        @endforeach
      </div>
        </div>
        </div>
      </table>
    </thead>
  </tbody>
  <a href="/" class="viewall">Back to previous page</a>
      @endsection
