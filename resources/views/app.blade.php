<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ELev-Jobs</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/heroic-features.css" rel="stylesheet">
</head>
<body>
  <nav class="navbar navbar-inverse">

<div class="container-fluid">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <img src="/img/logo.png" alt="Logo" />
  </div>

  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">

    <ul class="nav navbar-nav navbar-right">
      <li><a href="/auth/login">Login</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="#">Post Job</a></li>
    </ul>
  </div>
</div>
  </nav>
  <div class="container">

    @yield('content')

  </div>
</body>
</html>
