<!DOCTYPE html>
<html>
  <head>
    <title>ELev Jobs</title>
    <link rel="stylesheet" href="/css/bootstrap.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="/css/bootstrap.min.css" media="screen" title="no title" charset="utf-8">
  </head>
  <body>
    <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <img src="/img/logo.png" alt="Logo" />
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">

      <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Link</a></li>
      </ul>
    </div>
  </div>
</nav>
    <div class="mainContainer">
    <div class="col-md-6 container ">
      <h1 class="text-info col-sm-offset-2">Register here</h1>
      <br>
      <form class="form-horizontal">
        <div class="form-group">
          <label for="inputName" class=" col-sm-4 col-sm-offset-2 control-label">Name:</label>
          <div class="col-sm-5">
            <input type="text" class="form-control" id="inputName" placeholder="Enter your Name Here">
          </div>
        </div>
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-4 col-sm-offset-2  control-label">Email:</label>
          <div class="col-sm-5">
            <input type="email" class="form-control" id="inputEmail3" placeholder="Enter Your Mail Id">
          </div>
        </div>

        <div class="form-group">
          <label for="inputPassword4" class="col-sm-4 col-sm-offset-2  control-label">Type Password:</label>
          <div class="col-sm-5">
            <input type="password" class="form-control" id="inputPassword4" placeholder="Password">
          </div>
        </div>
        <div class="form-group">
          <label for="inputConfirmPassword" class="col-sm-4 col-sm-offset-2  control-label">Confirm Password:</label>
          <div class="col-sm-5 ">
            <input type="password" class="form-control" id="inputConfirmPassword" placeholder="Re-Type Password">
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success">Register</button>
          </div>
        </div>
      </form>
    </div>
      <div class="col-md-6 container">
        <br>
      <h1 class="text-info col-sm-offset-2">LOGIN</h1>
      <form class="form-horizontal">
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
    <div class="col-sm-5">
      <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
    <div class="col-sm-5">
      <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <div class="checkbox">
        <label>
          <input type="checkbox"> Remember me
        </label>
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-success">Sign in</button>
      <a href="#">forgot password?</a>
    </div>
  </div>
</form>

    </div>
  </div>
  </span>
  </body>
</html>
