@extends('app')
@section('content')
    <div class="container">
      <div class="row">
        <div class="col-md-11 col-md-offset-1">
        <div class="col-md-8"><h1 class="text-success">{{$job->title}}</h1><br>
        <div class="col-xs-4">posted on:sep-9-2015<br></div>
        <div class="col-xs-4">Company</div>
      </div>
      <div class="col-md-3"><img height="120px" src="/img/{{$job->organization->logo}}" alt="" /></div><br>
    <h2 class="text-primary">Description</h2>
    <p>
      {{$job->description}}
   </p>
<div class="container">
  <h2> Apply:</h2>
  @if($job->application_method=="email")
    {{$job->application_email->email}}
  @endif
  @if($job->application_method=="link")
    <a href="http://{{$job->application_link->link}}">{{$job->application_link->link}}</a>
  @endif
  @if($job->application_method=="form")
  <h2 class ="text-info">Company Form</h2>
  <form class="form-horizontal" action="/save" method="post">
    <input type="hidden" name="job_id" value="{{$job->id}}">
    <input type="hidden" name="fromEmail" value="{{$job->posted_by}}">
    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
    <div class="form-group">
      <label for="Name" class="col-sm-2 control-label">Name</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" name="name" placeholder="enter Name here">
      </div>
    </div>
    <div class="form-group">
      <label for="email" class="col-sm-2 control-label">Email</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" name="toEmail" id="" placeholder="enter email here">
      </div>
    </div>
    <div class="form-group">
      <label for="message" class="col-sm-2 control-label">Message</label>
      <div class="col-sm-6">
        <textarea name="message" rows="6" cols="60" placeholder="enter text here"></textarea>
      </div>
    </div>

    <div class="form-group">
      <label for="resume" class="col-sm-2 control-label">Resume</label>
      <div class="col-sm-6">
        <input type="file" name="attachment_file_name" value="">
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default">Submit</button>
      </div>
    </div>
  </form>
  confirm('Click OK to continue!');
  @endif
      </div>
        </div>
      </div>
    </div>
@endsection
