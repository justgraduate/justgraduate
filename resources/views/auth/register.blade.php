@extends('app')
@section('content')
<div class="col-md-12 container ">
  <h1 class="text-info col-sm-offset-4">Register here</h1>
  <br>
  @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif
  <form method="POST" action="/auth/register" class="form-horizontal">
    {!! csrf_field() !!}

    <div class="form-group">
      <label for="name" class=" col-sm-2 col-sm-offset-2 control-label">Name:</label>
      <div class="col-sm-5">
        <input type="text" class="form-control" name="name" id="inputName" placeholder="Enter your Name Here">
      </div>
    </div>
    <div class="form-group">
      <label for="email" class="col-sm-2 col-sm-offset-2  control-label">Email:</label>
      <div class="col-sm-5">
        <input type="email" class="form-control" id="inputEmail3" name="email" placeholder="Enter Your Mail Id">
      </div>
    </div>
    <div class="form-group">
      <label for="name" class=" col-sm-2 col-sm-offset-2 control-label">You are:</label>
      <div class="col-sm-5">
        <input type="radio" class="form" name="type" id="type" value="{{Config::get('constants.user_types.applicant')}}"> Applicant
        <input type="radio" class="form" name="type" id="type" value="{{Config::get('constants.user_types.employer')}}"> Employer
      </div>
    </div>
    <div class="form-group">
      <label for="inputPassword4" class="col-sm-2 col-sm-offset-2  control-label">Type Password:</label>
      <div class="col-sm-5">
        <input type="password" class="form-control" name="password" placeholder="Password">
      </div>
    </div>
    <div class="form-group">
      <label for="inputConfirmPassword" class="col-sm-2 col-sm-offset-2  control-label">Confirm Password:</label>
      <div class="col-sm-5 ">
        <input type="password" class="form-control" name="password_confirmation" placeholder="Re-Type Password">
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-offset-6 col-sm-2">
        <button type="submit" class="btn btn-success">Register</button>
      </div>
    </div>
  </form>
</div>


@endsection
