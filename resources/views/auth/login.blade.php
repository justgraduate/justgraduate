@extends('app')

@section('content')

<div class="col-md-12 container">
  <br>
  <h2 class="col-md-offset-3">New User? Click here to   <span> <a href="/auth/register">Register</a></span></h2>
<h1 class="text-info col-sm-offset-4">LOGIN</h1>
  @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif
<form method="POST" action="/auth/login" class="form-horizontal">
  {!! csrf_field() !!}

<div class="form-group">
<label for="email" class="col-sm-2 col-sm-offset-2 control-label">Email</label>
<div class="col-sm-5">
<input type="email" class="form-control" name="email" placeholder="Email">
</div>
</div>
<div class="form-group">
<label for="password" class="col-sm-2 col-sm-offset-2 control-label">Password</label>
<div class="col-sm-5">
<input type="password" class="form-control" id="inputPassword3" name="password" placeholder="Password">
</div>
</div>
<div class="form-group">
<div class="col-sm-offset-4 col-sm-8">
<div class="checkbox">
  <label>
    <input type="checkbox"> Remember me
  </label>
</div>
</div>
</div>
<div class="form-group">
<div class="col-sm-offset-4 col-sm-8">
<button type="submit" class="btn btn-success">Sign in</button>
<a href="#">forgot password?</a>
</div>
</div>
</form>

</div>




@endsection
