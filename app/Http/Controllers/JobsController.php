<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Job;
use App\UserSubmission;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class JobsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
  $programmingCategory = \Config::get('constants.categories.programming');
  $programmingJobs = Job::where("category",$programmingCategory)->orderBy("activated_at","desc")->take(10)->get();

  $designCategory = \Config::get('constants.categories.design');
  $designJobs = Job::where("category",$designCategory)->orderBy("activated_at","desc")->take(10)->get();

  $marketingCategory = \Config::get('constants.categories.marketing');
  $marketingJobs = Job::where("category",$marketingCategory)->orderBy("activated_at","desc")->take( 10)->get();
  return view('index',compact('programmingJobs','designJobs','marketingJobs'));
     }
    public function newjob()
    {
      return view('newjob');
    }
    public function viewall()
    {
      $jobs = Job::all();
      return view('viewall',compact('jobs'));
    }
    public function listAll($category)
    {
      $categoryVal = \Config::get('constants.categories.'.$category);
      $jobs = Job::where('category',$categoryVal)
      ->orderBy("activated_at","desc")->take(5)->get();
       return view('/viewall',compact('jobs','categoryVal'));
      // return $jobs ;
   }



    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    // public function usersubmission()
    // {
    //   $url = action('JobsController@store');
    //   return view('newjob',compact('url'));
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
    $application = new UserSubmission();
    $application->name = $request->input('name');
    $application->to_email= $request->input('toEmail');
    $application->message= $request->input('message');
    $application->attachment_file_name= $request->input('attachment_file_name');
    $application->job_id=$request->input('job_id');
    $application->applicant_id=1;
    $application->save();
    $redirecturl = action('JobsController@show',['posted_by'=>$application->job_id]);
     return redirect($redirecturl);

      // return Redirect::to('Job');
  }

    public function search(Request $request)
    {

     $category = $request->input('category');
      $jobs = Job::all();
      if(!empty($category)){
        $categoryVal = \Config::get('constants.categories.'.$category);
        $jobs = $jobs->filter(function($item) use($categoryVal) {
          return $item->category == $categoryVal;
        });
      }
      $location = $request->input('location');
      if(!empty($location)){
        $locationVal = \Config::get('constants.locations.'.$location);
        $jobs = $jobs->filter(function($item) use($locationVal) {
          return $item->location == $locationVal;
        });
      }
      $search = $request->input('search');
      if(!empty($search)){
        $jobs = $jobs->filter(function($item) use($search) {
        if((strpos($item->title,$search)!==false) || (strpos($item->description,$search)!==false))
          {
            return true;
          }
       });
      }
      $categoryVal = "Search Results";
      return view('/viewall',compact('jobs','categoryVal'));
    }


    /**

     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */


    public function show($id)
    {
      $job = Job::find($id);
      // return $job;
      return view('newjob',compact('job'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
