<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
      //
  protected $table = "jobs";

  public function application_link()
  {
    return $this->hasOne('App\ApplicationLink');
  }
  public function application_email()
  {
    return $this->hasOne('App\ApplicationEmail');
  }
  public function user_submissions()
  {
    return $this->hasMany('App\UserSubmission');
  }
  public function organization()
  {
    return $this->belongsTo('App\Organization');
  }
}
