<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSubmission extends Model
{
   
    public function users()
    {
      return $this->hasMany('App\UserSubmission');
    }
}
