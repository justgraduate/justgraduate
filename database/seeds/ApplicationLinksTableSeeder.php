<?php

use Illuminate\Database\Seeder;
use App\ApplicationLink;

class ApplicationLinksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('application_links')->delete();
      $json = File::get("database/data/application_links.json");
      echo $json;
      $data = json_decode($json);
      foreach($data as $obj) {
      ApplicationLink::create(array(
              'id' => $obj->id,
             'link' => $obj->link,
             'job_id' => $obj->job_id,
           ));
    }
}
}

?>
