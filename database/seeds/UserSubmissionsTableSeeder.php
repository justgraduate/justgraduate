<?php

use Illuminate\Database\Seeder;
use App\UserSubmission;
class UserSubmissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('user_submissions')->delete();
      $json = File::get("database/data/user_submissions.json");
      echo $json;
      $data = json_decode($json);
      foreach($data as $obj) {
      UserSubmission::create(array(
             'id' =>$obj->id,
             'applicant_id' =>$obj->applicant_id,
             'job_id' => $obj->job_id,
             'from_email' => $obj->from_email,
             'name' =>$obj->name,
             'to_email' =>$obj->to_email,
             'message' =>$obj->message ,
             'attachment_file_name' =>$obj->attachment_file_name,

           ));
    }
}
}
?>
