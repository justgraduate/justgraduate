<?php

use Illuminate\Database\Seeder;
use App\Organization;

class OrganizationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('organizations')->delete();
      $json = File::get("database/data/organization.json");
      echo $json;
      $data = json_decode($json);
      foreach($data as $obj) {
      Organization::create(array(
          'id'=>$obj->id,
          'website' => $obj->website,
          'name' => $obj->name,
          'logo' => $obj->logo,
));
      }
    }
}
?>
