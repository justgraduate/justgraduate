<?php

use Illuminate\Database\Seeder;
use App\OrganizationUser;

class OrganizationUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('organization_users')->delete();
      $json = File::get("database/data/organization_users.json");
      echo $json;
      $data = json_decode($json);
      foreach($data as $obj) {
      OrganizationUser::create(array(
          'id' => $obj->id,
          'user_id' => $obj->user_id,
          'organization_id' => $obj->organization_id,
          'role' => $obj->role,
        ));
      }
    }
  }
  ?>
