<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          DB::table('users')->delete();
          $json = File::get("database/data/user.json");
          echo $json;
          $data = json_decode($json);
          foreach($data as $obj) {
            User::create(array(
              'id'=>$obj->id,
              'email' => $obj->email,
              'name' => $obj->name,
              'password' => $obj->password,
              'type' => $obj->type,
            ));
          }
      }
    }
?>
