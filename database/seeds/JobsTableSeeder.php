<?php

use Illuminate\Database\Seeder;
use App\Job;
class JobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('jobs')->delete();
      $json = File::get("database/data/jobs.json");
      echo $json;
      $data = json_decode($json);
      foreach($data as $obj) {
      Job::create(array(
              'id' => $obj->id,
             'posted_by' => $obj->posted_by,
             'organization_id' =>$obj->organization_id,
             'title' => $obj->title,
             'description' =>$obj->description,
             'application_method' =>$obj->application_method,
             'category' => $obj->category,
             'location' => $obj->location,
             'status' =>$obj->status ,
             'activated_at' => $obj->activated_at,
             'expiry_at' =>$obj->expiry_at,
      ));
}
    }
}
