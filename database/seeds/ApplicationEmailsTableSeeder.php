<?php

use Illuminate\Database\Seeder;
use App\ApplicationEmail;
class ApplicationEmailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('application_emails')->delete();
      $json = File::get("database/data/application_emails.json");
      echo $json;
      $data = json_decode($json);
      foreach($data as $obj) {
      ApplicationEmail::create(array(
              'id' => $obj->id,
             'email' => $obj->email,
             'job_id' => $obj->job_id,
           ));
    }
}
}
