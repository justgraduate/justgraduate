<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('posted_by')->unsigned();
            $table->integer('organization_id')->unsigned();
            $table->string('title');
            $table->text('description');
            $table->string('application_method');
            $table->string('category');
            $table->string('location');
            $table->string('status');
            $table->dateTime('activated_at');
            $table->dateTime('expiry_at');
            $table->foreign('posted_by')->references('id')->on('users');
            $table->foreign('organization_id')->references('id')->on('organizations');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jobs');
    }
}
