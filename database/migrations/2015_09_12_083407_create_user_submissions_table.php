<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_submissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('applicant_id')->unsigned();
            $table->string('from_email');
            $table->integer('job_id')->unsigned();
            $table->string('name');
            $table->string('to_email');
            $table->text('message');
            $table->string('attachment_file_name');
            $table->foreign('job_id')->references('id')->on('jobs');
            $table->foreign('applicant_id')->references('id')->on('users');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_submissions');
    }
}
