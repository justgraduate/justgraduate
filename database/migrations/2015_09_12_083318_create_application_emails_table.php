<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_emails', function (Blueprint $table)
          {
            $table->increments('id');
            $table->integer('job_id')->unsigned();
            $table->string('email');
            $table->foreign('job_id')->references('id')->on('jobs');
            $table->timestamps();
        });
      }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('application_emails');
    }
}
