<?php

return [
  'user_types' => [
      'applicant' => 'APPLICANT',
      'employer' => 'EMPLOYER',
      'admin' => 'ADMIN'
    ],
    'categories' => [
      'design' => 'DESIGN',
      'programming' => 'PROGRAMMING',
      'marketing' => 'MARKETING'
   ],
  'locations' =>[
    'hyderabad' => 'Hyderabad',
    'bangalore' => 'Bangalore',
    'mumbai' => 'Mumbai',
    'delhi'  =>  'Delhi',
    'chennai' => 'Chennai',
    'jaipur' => 'Jaipur'
     ]
  ]
?>
